package com.practice.observer.observers;

import com.practice.observer.annotations.MyEventType;
import javax.enterprise.event.Observes;

/**
 *
 */
public class John  {
    public void doTask(@Observes @MyEventType(MyEventType.Type.IMPORTANT) final String instruction) {
        System.out.println("John is : " + instruction);
    }
}
