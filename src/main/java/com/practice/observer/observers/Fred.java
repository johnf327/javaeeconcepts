package com.practice.observer.observers;

import com.practice.observer.annotations.MyEventType;
import javax.enterprise.event.Observes;

/**
 *
 */
public class Fred {
    public void doTask(@Observes @MyEventType(MyEventType.Type.NON_IMPORTANT) final String instruction) {
        System.out.println("Fred is: " + instruction);
    }
}
