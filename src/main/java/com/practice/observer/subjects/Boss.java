package com.practice.observer.subjects;

import com.practice.observer.annotations.MyEventType;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 */
@Named
@RequestScoped
public class Boss {

    @Inject
    @MyEventType(MyEventType.Type.IMPORTANT)
    Event<String> importantEvent;

    @Inject
    @MyEventType(MyEventType.Type.NON_IMPORTANT)
    Event<String> nonImportantEvent;

    public void triggerImportantEvents() {
        importantEvent.fire("important event");
    }

    public void triggerNonImportantEvents() {
        nonImportantEvent.fire("non important event");
    }
}
