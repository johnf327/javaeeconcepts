package com.practice.observer.subjects;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Named;

/**
 *
 */
@Named
@ApplicationScoped
public class DoesSomethingOnStartUp {

    public void init(@Observes @Initialized(ApplicationScoped.class) final Object init) {
        System.out.println("I EAT GOATS!!!!!");
    }
}
